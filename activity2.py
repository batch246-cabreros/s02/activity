year = int(input("Enter a year: "))

if year % 4 == 0:
    if year % 100 == 0:
        if year % 400 == 0:
            print(year, "is a leap year")
        else:
            print(year, "is not a leap year")
    else:
        print(year, "is a leap year")
else:
    print(year, "is not a leap year")


print("====================")

row = int(input("Enter number of rows: "))
col = int(input("Enter number of columns: "))

for i in range(row):
    for j in range(col):
        print("*", end=" ")
    print()
